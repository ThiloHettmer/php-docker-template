# PHP Docker Template v1.0

## Description

This project is a template for further use, to help developers with their PHP application.

Following technologies are included:

- PHP 7.4
- MySQL 5.7
- PHPMyAdmin
- nginx web server

They are all running in separate docker containers.

To help you with your app, this project already got `behat` and `phpunit` included.

## Requirements

- Git
- Docker --version 19.03.6
- Docker-Compose --version 1.26.2
- Make

## Installation

- Clone this project to a folder you like
- Create a `.env` file in the root project folder.
- Copy the content of `.env.example` to `.env`. IMPORTANT: Change the passwords for production !!
- Run `make` to build the project.

## Run the app

- Run `make up` or `make daemon` to start the app.
- Note: If you are starting the app for the first time, you will need to run `make composer` after starting the 
containers.

## Tests

All tests are located in the `app/tests` folder. 

**Behat**: Put your behat features inside `app/tests/behat/features` and the tests inside `app/tests/behat/bootstrap`. 
Don't forget to update the `app/tests/behat/behat.yml`! You can run them by using the `make behat` command. There is 
already a basic test included (testContext), just to check if behat is working properly.

**PHPUnit**: Just add your PHPUnit tests to the `app/tests/phpunit` folder. 
Run them by using the `make phpunit` command.

## Gitlab-CI

If you don't want to use the Gitlab CI/CD, just delete or rename the `.gitlab-ci.yml` file.

There are already 3 Stages implemented into this template:

**build**: Builds the images (production image only to master branch)

**test**: Runs all PHPUnit and Behat Tests located in the `app/test` folder

**deploy**: Deploys the production image to a server of your choice. Note: You have to configure this 
step by yourself or if you don't want it, just remove this step from the `.gitlab-ci.yml` file.

### Required Steps for Deploying this App

You will need to create a file-variable in the Gitlab CI/CD and paste the content of your `.env` file into it. 
**Don't use the example password and ports! Change it for production!**

After deploying your app, you should not build the other images right away with `make` because we already have build the
production image. Instead create another docker-compose.yml without the PMA_USER and PMA_PASSWORD entries in the 
phpmyadmin section and build the other images with `docker-compose build webserver phpmyadmin database`. 

### Example Deploy Step
```
deploy:
  stage: deploy
  only:
    - master
  before_script:
    - apk add rsync bash
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - bash -c 'ssh-add <(echo "$DEPLOY_KEY_PRIVATE")'
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan ${DEPLOY_HOST} >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ssh ${DEPLOY_USER}@${DEPLOY_HOST} "docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY"
    - ssh ${DEPLOY_USER}@${DEPLOY_HOST} "docker pull $CI_REGISTRY_IMAGE:latest"
    - rsync -azh $ENV_VARIABLES ${DEPLOY_USER}@${DEPLOY_HOST}:/srv/env/.env
  after_script:
    - rm -r ~/.ssh
```

In this example we use the following variables that are preset in the Gitlab CI/CD ui:

- $DEPLOY_KEY_PRIVATE: This is the private ssh key from our server
- $DEPLOY_HOST: The ip or hostname from our server
- $DEPLOY_USER: The user on our server
- $ENV_VARIABLES: In this example we use another `.env` for our application. (Usually located in app/.env)

What does this deploy script do?

Before Script:

1) First we install rsync and bash. (needed for the .env sync)
2) Install openssh-client if not already installed
3) Check if ssh-agent is installed
4) Add DEPLOY_KEY_PRIVATE to ssh
5) Create a .ssh folder
6) Set permissions of the .ssh folder
7) Run a key scan and add this key ot the known_hosts
8) Set permissions of the known_hosts folder

Script:
1) Login to gitlab via gitlab-ci-token on our server.
2) Pull latest image from gitlab to our server.
3) Sync the content of the $ENV_VARIABLES variable to our server.

After Script:
1) Cleanup

## Useful commands

`make` build the docker images

`make up` start all docker containers

`make daemon` start all docker containers in daemon mode

`make stop` stop all docker containers

`make down` remove all docker containers

`make bash` create a bash inside the workspace container

`make phpunit` run phpunit tests

`make behat` run behat tests

`make composer` run `composer install` inside the workspace container

`make composer-autoload` run `composer dump-autoload -o` inside the workspace container

## Author & Contact

This project was created by Thilo Hettmer. 

Gitlab: https://gitlab.com/DerThilo

If you have any suggestions, feel free to create an issue here: https://gitlab.com/DerThilo/php-docker-template/-/issues

Special thanks to @DerMaddin for helping with the Gitlab Pipeline

## Licence

MIT License

Copyright (c) 2020 Thilo Hettmer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
