build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 UID=$(shell id -u) GID=$(shell id -g) docker-compose build
up:
	@docker-compose up
daemon:
	@docker-compose up -d
stop:
	@docker-compose stop
down:
	@docker-compose down
bash:
	@docker-compose exec app bash
phpunit:
	@docker-compose run app php ./vendor/bin/phpunit tests/phpunit
behat:
	@docker-compose run app ./vendor/bin/behat --config tests/behat/behat.yml
composer:
	@docker-compose run app composer install
composer-autoload:
	@docker-compose exec app composer dump-autoload -o