FROM composer:latest AS composer

# base
FROM php:7.4-fpm as base

# Pass user to docker container
ARG USERNAME=base
ARG UID=1000
ARG GID=1000
RUN groupadd -g $GID -o $USERNAME
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $USERNAME

# Update and install dependencies
RUN apt-get update -y \
    && apt-get upgrade -y && apt-get install -y libzip-dev unzip \
    && docker-php-ext-install pdo_mysql zip

# Create app dir and set user permissions
RUN mkdir /usr/src/app

# Set user dir permissions
RUN chown -R $USERNAME:$USERNAME /usr/src/app

# Configure php-fpm to work together with nginx
# We name the config zzz since on the container is already a zz-docker.conf
COPY /config/php-fpm.conf /usr/local/etc/php-fpm.d/zzz-docker.conf

WORKDIR /usr/src/app

# Add composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# Stage: workspace
FROM base as workspace

USER $USERNAME

CMD ["php-fpm"]

# Stage: ci-test
FROM base as ci-test

# Copy app
COPY /app /usr/src/app

# Install composer dependencies
RUN composer install \
    --optimize-autoloader \
    --no-interaction \
    --no-progress \
    && composer dump-autoload -o

USER $USERNAME

CMD ["php-fpm"]

# Stage: production
FROM base as production
# Expose port 9000 and start php-fpm server

# Copy app
COPY /app /usr/src/app

# Install composer dependencies without dev dependencies
RUN composer install \
    --no-dev \
    --optimize-autoloader \
    --no-interaction \
    --no-progress \
    && composer dump-autoload -o

# Remove composer from production image to decrease image size
RUN rm -R /usr/bin/composer

USER $USERNAME

EXPOSE 9000
CMD ["php-fpm"]