<?php

use Behat\Behat\Context\Context;

/**
 * Class testContext
 */
class testContext implements Context
{
    /**
     * @Given there is a file :arg1
     * @param $arg1
     * @throws Exception
     */
    public function doesFileExists(string $arg1): void
    {
        if (!file_exists($this->getProjectPath().$arg1)) {
            throw new Exception('File '.$arg1.' does not exist');
        }
    }

    /**
     * @return string
     */
    private function getProjectPath(): string
    {
        return __DIR__.'/../../../';
    }
}